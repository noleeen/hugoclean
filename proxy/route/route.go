package route

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/redis/go-redis/v9"
	"hugoclean/proxy/config"
	"hugoclean/proxy/internal/entities/authEntity"
	"hugoclean/proxy/internal/infrastructure/middleware"
	"hugoclean/proxy/internal/modules/auth/controller"
	"hugoclean/proxy/internal/modules/auth/service"
	controller2 "hugoclean/proxy/internal/modules/geo/controller"
	"log"
	"net/http"
	"net/http/pprof"
)

func NewApiRouter() http.Handler {

	cfg := config.NewConfig()

	//postgresql
	dsn := cfg.GetDsnDB()
	fmt.Println("dsn", dsn)

	db, err := sql.Open(cfg.DB.Driver, dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	//
	r := chi.NewRouter()

	proxy := middleware.NewReverseProxy("hugo", "1313")
	r.Use(proxy.ReverseProxy)

	//авторизация
	authStorage := &authEntity.StorageAuth{UsersMap: map[string]authEntity.User{}}
	token := jwtauth.New("HS256", []byte("randomPass"), nil)

	authService := service.NewAuthService(authStorage, token)
	authController := controller.NewAuthController(authService)

	//вот здесь запустим редис для быстрого кэширования запросов
	clientRedis := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
		DB:   0,
	})
	ctx := context.Background()
	// Проверка соединения с Redis
	pong, err := clientRedis.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ошибка соединения с Redis:", err)
		log.Println(err)
	}
	fmt.Println("Соединение с Redis успешно:", pong)

	//наша бизнес-логика (геолокация)
	geoController := controller2.NewGeoController(db, clientRedis, cfg)

	r.Handle("/metrics", promhttp.Handler())

	r.Route("/api", func(r chi.Router) {
		r.Get("/*", HelloFromApi)

		r.Post("/login", authController.Login)
		r.Post("/register", authController.Register)
		r.Route("/address", func(r chi.Router) {
			r.Use(jwtauth.Verifier(token))
			r.Use(jwtauth.Authenticator)
			// по адресу http://localhost:8080/address/search/ пишем в поисковой строке приблизительный адрес,
			//с помощью geoservice.SearchAddressHandler подирается предполагаемый адрес и показывается на карте.
			//нажимаем на маркер на карте и с помощью geoservice.GeocodeAddressHandler нам показывается список из точных адресов в радиусе 75 метров
			r.Post("/search", geoController.SearchAddressHandler)
			r.Post("/geocode", geoController.GeocodeAddressHandler)
		})
	})

	r.Route("/debug", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(token))
			r.Use(jwtauth.Authenticator)
			r.Group(func(r chi.Router) {
				r.HandleFunc("/pprof", pprof.Index)
				r.HandleFunc("/cmdline", pprof.Cmdline)
				r.HandleFunc("/profile", pprof.Profile)
				r.HandleFunc("/symbol", pprof.Symbol)
				r.HandleFunc("/trace", pprof.Trace)
				r.Handle("/allocs", pprof.Handler("allocs"))
				r.Handle("/block", pprof.Handler("block"))
				r.Handle("/goroutine", pprof.Handler("goroutine"))
				r.Handle("/heap", pprof.Handler("heap"))
				r.Handle("/mutex", pprof.Handler("mutex"))
				r.Handle("/threadcreate", pprof.Handler("threadcreate"))
			})
		})
	})

	return r
}

func HelloFromApi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from API"))
}
