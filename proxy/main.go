package main

import (
	"context"
	"fmt"
	"hugoclean/proxy/route"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	//if err := godotenv.Load(); err != nil {
	//	log.Fatalf("Error loading .env file: %v", err)
	//}

	r := route.NewApiRouter()

	//go workers.WorkerTest()

	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	//создаем канал для принятия сигналов о завершении работы
	signChan := make(chan os.Signal, 1)
	//функция signal.Notify для регистрации обработчика сигналов
	signal.Notify(signChan, syscall.SIGINT, syscall.SIGTERM)

	//запускаем сервер в горутине
	go func() {
		fmt.Println("server is running on :8080")
		err := server.ListenAndServe()
		if err != nil {
			log.Fatalf("Server error: %v", err)
		}
	}()

	//ожидаем сигнал завершения работы
	<-signChan
	fmt.Println("Shutting down server...")

	// Ожидаем завершения всех активных запросов в течение 5 секунд
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		fmt.Printf("Error during server shutdown: %v\n", err)
	}

	fmt.Println("Server gracefully stopped")
}
