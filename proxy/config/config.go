package config

import (
	"fmt"
	"os"
)

type Config struct {
	DB         DB
	DadataKeys DDKey
}

type DB struct {
	Driver   string
	Host     string
	Port     string
	Username string
	Password string
	DbName   string
	SslMode  string
}

var defaultDbConfig = DB{
	Driver:   "postgres",
	Host:     "localhost",
	Port:     "5432",
	Username: "postgres",
	Password: "secret",
	DbName:   "users",
}

type DDKey struct {
	ApiKey    string
	SecretKey string
}

func NewConfig() *Config {
	return &Config{
		DB:         getDbConfig(),
		DadataKeys: getDadataKeys(),
	}
}

func (c *Config) GetDsnDB() string {

	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		c.DB.Host,
		c.DB.Port,
		c.DB.Username,
		c.DB.Password,
		c.DB.DbName,
		c.DB.SslMode,
	)

}

func getDadataKeys() DDKey {
	d := DDKey{}
	d.ApiKey = os.Getenv("API_KEY_VALUE")
	d.SecretKey = os.Getenv("SECRET_KEY_VALUE")
	return d
}

func getDbConfig() DB {
	db := DB{}
	db.Driver = getEnv("DB_DRIVER", defaultDbConfig.Driver)
	db.Host = getEnv("DB_HOST", defaultDbConfig.Host)
	db.Port = getEnv("DB_PORT", defaultDbConfig.Port)
	db.Username = getEnv("DB_USER", defaultDbConfig.Username)
	db.Password = getEnv("DB_PASSWORD", defaultDbConfig.Password)
	db.DbName = getEnv("DB_NAME", defaultDbConfig.DbName)
	db.SslMode = getEnv("SSL_MODE", defaultDbConfig.SslMode)
	return db
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}
