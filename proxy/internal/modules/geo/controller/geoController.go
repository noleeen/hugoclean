package controller

import (
	"database/sql"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"hugoclean/proxy/config"
	"hugoclean/proxy/internal/entities/geoEntity"
	"hugoclean/proxy/internal/infrastructure/responder"
	"hugoclean/proxy/internal/modules/geo/service"
	"hugoclean/proxy/metrics"
	"io"
	"net/http"
	"time"
)

type GeoController struct {
	service    service.GeoServicer
	respond    responder.Responder
	cacheRedis *redis.Client
}

func NewGeoController(db *sql.DB, cacheRedis *redis.Client, conf *config.Config) *GeoController {
	return &GeoController{
		service: service.NewGeoService(db, cacheRedis, conf),
		respond: responder.NewResponder(),
	}
}

func (g *GeoController) SearchAddressHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.SearchAddressRequestCount.Inc()
		metrics.SearchAddressDurationCount.Observe(float64(time.Since(start)))
	}()

	var requestByUser geoEntity.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&requestByUser)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	searchResponse, err := g.service.PrepareSearchRequest(requestByUser)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	g.respond.OutputJSON(w, searchResponse)
}

func (g *GeoController) GeocodeAddressHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		metrics.GeocodeRequestCount.Inc()
		metrics.GeocodeDurationCount.Observe(float64(time.Since(start)))
	}()

	var request geoEntity.GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		g.respond.ErrorBadRequest(w, err)
	}

	geocodeRequest, err := g.service.PrepareGeocodeRequest(request, "75")
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	newClient := &http.Client{}

	response, err := g.service.DoRequest(newClient, geocodeRequest)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}
	defer response.Body.Close()

	respBody, err := io.ReadAll(response.Body)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	var suggestions geoEntity.Suggestions
	err = json.Unmarshal(respBody, &suggestions)
	if err != nil {
		g.respond.ErrorInternal(w, err)
	}

	addresses := &geoEntity.GeocodeResponse{}
	for _, adr := range suggestions.Suggestions {
		addresses.Addresses = append(addresses.Addresses,
			&geoEntity.Address{
				Lat:    *adr.Data["geo_lat"],
				Lon:    *adr.Data["geo_lon"],
				Result: adr.Value,
			})
	}

	g.respond.OutputJSON(w, addresses)
}
