package service

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/ekomobile/dadata/v2"
	"github.com/ekomobile/dadata/v2/client"
	"github.com/redis/go-redis/v9"
	"hugoclean/proxy/config"
	"hugoclean/proxy/internal/entities/geoEntity"
	"hugoclean/proxy/internal/modules/geo/repository"
	"hugoclean/proxy/metrics"
	"log"
	"net/http"
	"time"
)

type GeoService struct {
	storage    repository.GeoStorager
	cacheRedis *redis.Client
	conf       *config.Config
}

func NewGeoService(db *sql.DB, cacheRedis *redis.Client, conf *config.Config) GeoServicer {
	return &GeoService{
		storage:    repository.NewGeoStorage(db),
		cacheRedis: cacheRedis,
		conf:       conf,
	}
}

func (a *GeoService) PrepareGeocodeRequest(coordinates geoEntity.GeocodeRequest, radius string) (*http.Request, error) {
	//ОШИБКА в скрипте search.md в папке Hugo. там вместо lon указано lng, а с lng не считывается dadata
	//по-хорошему нужно исправить код фронта на search.md, но я там не особо разобрался,
	//поэтому сделал структуру-адаптер которая с реквеста считывает lng записывает в lon и отправляет на dadata

	adapter := &geoEntity.Adapter{
		Lat:           coordinates.Lat,
		Lon:           coordinates.Lng,
		Radius_meters: radius,
	}

	marshal, err := json.Marshal(adapter)
	if err != nil {
		return nil, err
	}
	url := "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
	requestByDadata, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(marshal))
	if err != nil {
		return nil, err
	}

	requestByDadata.Header.Add("Content-Type", "application/json")
	requestByDadata.Header.Add("Accept", "application/json")
	requestByDadata.Header.Add("Authorization", "Token a946c983104305a9207502be79c394f0128cc0ff")

	return requestByDadata, nil
}

func (a *GeoService) PrepareSearchRequest(query geoEntity.SearchRequest) (*geoEntity.SearchResponse, error) {

	var searchResponse geoEntity.SearchResponse

	ctx := context.TODO()

	startReadCache := time.Now() //metrics1---------------------------------------

	result, err := a.cacheRedis.Get(ctx, query.Query).Result()
	metrics.ReadCache.Observe(float64(time.Since(startReadCache))) //metric1-------
	if err == redis.Nil {
		// Если данных нет в кэше, выполняем запрос к базе данных
		fmt.Println("Данных нет в кэше. Выполняем запрос к базе данных...")

		startReadDB := time.Now() //metric2---------------------------------------
		address, err2 := a.storage.FindWithLevenshtein(query.Query)
		metrics.ReadDB.Observe(float64(time.Since(startReadDB))) //metric2---------
		if err2 != nil {
			return nil, err2
		}

		//если в базе данных есть данные по нашему текущему запросу, записываем их в нашу структуру ответа
		if address != nil {
			searchResponse.Addresses = []*geoEntity.Address{{
				Lat: address.Lat,
				Lon: address.Lon,
				//Result: "", TODO
			}}

			//записываем в кэш результат который пришёл из базы данных по нашему запросу
			marshal, err3 := json.Marshal(&searchResponse)
			if err3 != nil {
				fmt.Println("ошибка маршализации данных для кэша:", err3)
				return nil, err3
			}
			startWriteCache := time.Now() //metric3-------------------------------------
			err4 := a.cacheRedis.Set(ctx, query.Query, marshal, 5*time.Minute).Err()
			metrics.WriteCache.Observe(float64(time.Since(startWriteCache))) //metric3---
			if err4 != nil {
				fmt.Println("ошибка записи данных в кэш:", err4)
				return nil, err4
			}

			return &searchResponse, nil
		}
	} else if err == nil {
		//если нет ошибок значит данные есть в кэше, извлекаем их оттуда и возвращаем из функции
		err2 := json.Unmarshal([]byte(result), &searchResponse)
		if err2 != nil {
			fmt.Println("ошибка анмаршалинга данных из кэша:", err2)
			return nil, err2
		}
		fmt.Println("данные из кэша!")
		return &searchResponse, nil
	}

	//если в кэше и базе не было данных, получаем их из сервиса:
	startRequestToApi := time.Now() //metric4----------------------------------------
	api := dadata.NewCleanApi(client.WithCredentialProvider(&client.Credentials{
		//ApiKeyValue:    "a946c983104305a9207502be79c394f0128cc0ff",
		//SecretKeyValue: "4cabaeda3a91e75d2a16c1d74aea1203237e3d3b",
		ApiKeyValue:    a.conf.DadataKeys.ApiKey,
		SecretKeyValue: a.conf.DadataKeys.SecretKey,
	}))
	metrics.RequestToApiDadata.Observe(float64(time.Since(startRequestToApi))) //metric4---

	addresses, err := api.Address(context.Background(), query.Query)
	if err != nil {
		return &geoEntity.SearchResponse{}, err
	}

	searchResponse.Addresses = []*geoEntity.Address{{
		Lat:    addresses[0].GeoLat,
		Lon:    addresses[0].GeoLon,
		Result: addresses[0].Result,
	}}

	//записываем полученные данные в базу данных
	startWriteDB := time.Now() //metric5----------------------------------------------
	err = a.storage.AddAddressQueryLink(addresses[0].Result, addresses[0].GeoLat, addresses[0].GeoLon)
	metrics.WriteDB.Observe(float64(time.Since(startWriteDB))) //metric5--------------
	if err != nil {
		log.Println(err)
		fmt.Println("error add data to database:", err)
	}

	//записываем полученные данные в кэш
	marshal, err := json.Marshal(&searchResponse)
	if err != nil {
		fmt.Println("ошибка маршализации данных для кэша:", err)
		return nil, err
	}
	startWriteCache2 := time.Now() //metric6------------------------------------------
	err = a.cacheRedis.Set(ctx, query.Query, marshal, 5*time.Minute).Err()
	metrics.WriteCache.Observe(float64(time.Since(startWriteCache2))) //metric6-------
	if err != nil {
		fmt.Println("ошибка записи данных в кэш:", err)
		return nil, err
	}

	return &searchResponse, nil
}

func (a *GeoService) DoRequest(client *http.Client, request *http.Request) (*http.Response, error) {
	response, err := client.Do(request)
	return response, err
}
