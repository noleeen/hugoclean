package service

import (
	"hugoclean/proxy/internal/entities/geoEntity"
	"net/http"
)

type GeoServicer interface {
	PrepareGeocodeRequest(coordinates geoEntity.GeocodeRequest, radius string) (*http.Request, error)
	PrepareSearchRequest(query geoEntity.SearchRequest) (*geoEntity.SearchResponse, error)
	DoRequest(client *http.Client, request *http.Request) (*http.Response, error)
}
