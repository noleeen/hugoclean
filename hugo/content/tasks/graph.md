---
menu:
    after:
        name: graph
        weight: 1
title: Построение графа
---

# Построение графа: 
{{< columns >}}
{{< mermaid >}}
graph LR
0>name0]-->2((name2))
0>name0]-->3[name3]
0>name0]-->4{name4}
0>name0]-->7{name7}
0>name0]-->10[name10]
12((name12))-->0>name0]
0>name0]-->16(name16)
0>name0]-->17(name17)
1((name1))-->2((name2))
1((name1))-->5{name5}
1((name1))-->8(name8)
1((name1))-->10[name10]
11[name11]-->1((name1))
18>name18]-->1((name1))
5{name5}-->2((name2))
7{name7}-->2((name2))
9{{name9}}-->2((name2))
2((name2))-->15{name15}
3[name3]-->7{name7}
3[name3]-->9{{name9}}
10[name10]-->3[name3]
12((name12))-->3[name3]
17(name17)-->3[name3]
3[name3]-->18>name18]
4{name4}-->6(name6)
4{name4}-->8(name8)
4{name4}-->9{{name9}}
4{name4}-->11[name11]
4{name4}-->14([name14])
16(name16)-->4{name4}
4{name4}-->18>name18]
5{name5}-->8(name8)
5{name5}-->9{{name9}}
5{name5}-->17(name17)
5{name5}-->19([name19])
6(name6)-->7{name7}
10[name10]-->6(name6)
12((name12))-->6(name6)
6(name6)-->13{name13}
14([name14])-->6(name6)
15{name15}-->6(name6)
16(name16)-->6(name6)
17(name17)-->6(name6)
7{name7}-->8(name8)
9{{name9}}-->7{name7}
7{name7}-->11[name11]
7{name7}-->14([name14])
18>name18]-->7{name7}
19([name19])-->7{name7}
10[name10]-->8(name8)
8(name8)-->13{name13}
18>name18]-->8(name8)
8(name8)-->19([name19])
10[name10]-->9{{name9}}
9{{name9}}-->13{name13}
9{{name9}}-->15{name15}
9{{name9}}-->16(name16)
9{{name9}}-->17(name17)
9{{name9}}-->19([name19])
10[name10]-->11[name11]
12((name12))-->10[name10]
10[name10]-->13{name13}
11[name11]-->13{name13}
12((name12))-->18>name18]
19([name19])-->12((name12))
13{name13}-->15{name15}
18>name18]-->14([name14])
17(name17)-->15{name15}
15{name15}-->18>name18]
17(name17)-->16(name16)
16(name16)-->18>name18]
17(name17)-->18>name18]

{{< /mermaid >}}
{{< /columns >}}